#!/usr/bin/env python

import torch

# MODEL
W = 20

model = torch.nn.Sequential(
    torch.nn.Linear(1,W),
    torch.nn.Sigmoid(),
    torch.nn.Linear(W,W),
    torch.nn.Sigmoid(),
    torch.nn.Linear(W,1),
)

opt = torch.optim.Adam(model.parameters(), lr=1e-3)

def estimate_loss(m, K=10000):
    # Construct the generating monomials
    gen = torch.poisson(torch.ones(K,1)*4.)
    term1 = gen*m(gen-1) # TODO not if gen is 0
    term2 = -2*m(gen+1)
    term3 = -4*m(gen+3)
    loss = (sum(abs(term1+term2+term3)**2)/(m(torch.tensor([[0.]])))**2)/K
    return loss

def single(x):
    return torch.tensor([[x]])

def expect(x):
    return (model(single(x)) / model(single(0.))).item()

while True:
    loss = estimate_loss(model)
    opt.zero_grad()
    print(loss.item(), expect(1.), expect(2.), expect(3.), expect(4.))
    loss.backward()
    opt.step()

