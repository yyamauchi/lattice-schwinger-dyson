#Created on 4/19/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions with arbitrary # of sites
#, purely imaginary time
#On 4/21/2021, tested with 3 sites that it converges to right expectation values when mu > J, 
# but loss function doesn't get smaller when j > mu 


# Files
import torch
from torch import nn
import numpy as np
import math
import random


# Physics setting
J = 0.5             #coupling
mu = 0.5             #magnetic
N = 3                #Number of temporal sites
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)

# Model, neural network
class Model(nn.Module):
	def __init__(self):
		super(Model, self).__init__()
		self.flatten = nn.Flatten()
		self.linear_relu_stack = nn.Sequential(
			nn.Linear(N,5),
			nn.ReLU(), 
			nn.Linear(5,10),
			nn.ReLU(),
			nn.Linear(10,15),
			nn.ReLU(),
			nn.Linear(15,1)
			)

	def forward(self, x):
		x = self.flatten(x)
		logits = self.linear_relu_stack(x)
		return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations

triv = torch.zeros((1,N))
def loss_estimate(m, i, size, mode):
	loss = torch.zeros((size,1))

	for k in range(size):
		x = np.random.randint(N)
		iv = i[k]
		v1 = np.mod(iv+vec[x],2)
		v2 = np.mod(iv+vec[x]+vec[np.mod(x+1,N)],2)
		v3 = np.mod(iv+vec[np.mod(x+1,N)],2)
		v4 = np.mod(iv+vec[np.mod(x-1,N)]+vec[x],2)
		v5 = np.mod(iv+vec[np.mod(x-1,N)],2)
		v6 = np.mod(iv+vec[np.mod(x-1,N)]+vec[np.mod(x+1,N)],2)
		v7 = np.mod(iv+vec[np.mod(x-1,N)]+vec[x]+vec[np.mod(x+1,N)],2)


		if i[k,0,x]==0:
			loss[k,0] = ((1-cj*cj*cmu)*m(iv) - cj*cj*smu*m(v1) - cj*sj*cmu*m(v2) - cj*sj*smu*m(v3)
			- sj*cj*cmu*m(v4) - sj*cj*smu*m(v5) - sj*sj*cmu*m(v6) - sj*sj*smu*m(v7))/m(triv)
		else:
			loss[k,0] = ((1+cj*cj*cmu)*m(iv) + cj*cj*smu*m(v1) + cj*sj*cmu*m(v2) + cj*sj*smu*m(v3)
			+ sj*cj*cmu*m(v4) + sj*cj*smu*m(v5) + sj*sj*cmu*m(v6) + sj*sj*smu*m(v7))/m(triv)

		if mode ==1:
			print(x)
			print(iv, v1, v2, v3, v4, v5, v6, v7)
			print(loss[k,0].item()*m(triv))

	if mode ==2:
		print(loss*m(triv))

	tloss =0
	for j in range(size):
		tloss += abs(loss[j,0])/float(size)

	return tloss


# functions for training and testing
vec = torch.zeros((N,1,N,))
for k in range(N):
	vec[k,0,k] = 1

trainsize = 100

def train_loop(m, optimizer):
	#Create random monomial
	mn = torch.zeros((trainsize,1,N,))
	for i in range(trainsize):
		for j in range(N):
			mn[i,0,j] = np.random.randint(2)


	# Compute loss and optimize
	optimizer.zero_grad()
	Tloss = loss_estimate(m, mn, trainsize,0)
	Tloss.backward()
	optimizer.step()

testsize = 100

def test_loop(m):
	#Create random monomial
	mn = torch.zeros((testsize,1,N,))
	for i in range(testsize):
		for j in range(N):
			mn[i,0,j] = np.random.randint(2)
    
    # Compute loss and optimize
	Loss = loss_estimate(m, mn, testsize,0)

	print(Loss)
	return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate)

goal = 100.0
trial = 0
# input = torch.tensor([[[0.,0.]],[[0.,1.]],[[1.,0.]],[[1.,1.]]])
input = torch.tensor([[[0.,0.,0.]],[[0.,0.,1.]],[[0.,1.,0.]],[[0.,1.,1.]],[[1.,0.,0.]],[[1.,0.,1.]],[[1.,1.,0.]],[[1.,1.,1.]]])



while goal > 1e-3:
	trial += 1
	train_loop(model, optimizer)
	if trial % 30 == 0:
		print(f"{trial}th loop ------------------")
		goal = test_loop(model)
		pred = model(input)
		# print(pred[0].item(),pred[1].item()/pred[0].item(),pred[2].item()/pred[0].item(),pred[3].item()/pred[0].item())
		print(pred[0].item(),pred[1].item()/pred[0].item(), pred[2].item()/pred[0].item(), pred[3].item()/pred[0].item(), pred[4].item()/pred[0].item(), pred[5].item()/pred[0].item(), pred[6].item()/pred[0].item(), pred[7].item()/pred[0].item())







