#Created on 4/29/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions, purely imaginary time


# Files
import torch
from torch import nn
import numpy as np
import math
import random


# Physics setting
J = 1.0             #coupling
mu = 0.1             #magnetic
N = 1                #Number of temporal sites

# Model, neural network
class Model(nn.Module):
	def __init__(self):
		super(Model, self).__init__()
		self.flatten = nn.Flatten()
		self.linear_relu_stack = nn.Sequential(
            nn.Linear(N,N+10),
            nn.ReLU(),
            nn.Linear(N+10, N+20),
            nn.Sigmoid(),
            nn.Linear(N+20,1)
		)

	def forward(self, x):
		x = self.flatten(x)
		logits = self.linear_relu_stack(x)
		return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations
def loss_estimate(m, i, size):

    loss = torch.zeros((size,1,))
    for k in range(size):
    	if i[k,0,0]==0:
    		loss[k,0] = 1. - m(i[k])
    	else:
    		loss[k,0] = np.sinh(-2*mu) + m(i[k]) * (1 + np.cosh(-2*mu))

    tloss =0
    for j in range(size):
    	tloss += abs(loss[j,0])/float(size)

    return tloss


# functions for training and testing
vec = torch.zeros((N,1,N,))
for k in range(N):
	vec[k,0,k] = 1

trainsize = 10

def train_loop(m, optimizer):
	#Create random monomial
	mn = torch.zeros((trainsize,1,N,))
	for i in range(trainsize):
		for j in range(N):
			mn[i,0,j] = np.random.randint(2)

	# Compute loss and optimize
	optimizer.zero_grad()
	Tloss = loss_estimate(m, mn, trainsize)
	Tloss.backward()
	optimizer.step()

testsize = 1000

def test_loop(m):
	#Create random monomial
	mn = torch.zeros((testsize,1,N,))
	for i in range(testsize):
		for j in range(N):
			mn[i,0,j] = np.random.randint(2)

    # Compute loss and optimize
	Loss = loss_estimate(m, mn, testsize)

	print(Loss)
	return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate)
goal = 100.0
trial = 0

while goal > 1e-2:
	trial += 1
	train_loop(model, optimizer)
	if trial % 100== 0:
		print(f"{trial}th loop ------------------")
		goal = test_loop(model)
		input = torch.tensor([[[0.]],[[1.]]])
		pred = model(input)
		print(pred[0].item(), pred[1].item(), (-np.sinh(-2*mu))/(1+np.cosh(-2*mu)))

