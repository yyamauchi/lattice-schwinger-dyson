#!/usr/bin/env python

import numpy as np

K = 5

A = np.zeros((K+3,K+3))

for k in range(K):
    if k > 0:
        A[k,k-1] = k
        A[k,k+1] = -2
        A[k,k+3] = -4
A[K,K] = 1
A[K+1,0] = -.1
A[K+1,K+1] = 1
A[K+2,K+2] = 1
# TODO that truncation really uses the fact that K is odd.


vals, vecs = np.linalg.eigh(A.conj().T @ A)

#print(vals)

#print(vecs[:,0])
print(vecs[1,0]/vecs[0,0])
print(vecs[2,0]/vecs[0,0])
print(vecs[3,0]/vecs[0,0])
print(vecs[4,0]/vecs[0,0])

