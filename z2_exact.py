import numpy as np
import math
from operator import add
from numpy import linalg as LA

# Physics parameters
N = 12            # Number of sites
J = 0.3
mu = 0.3
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)
c0 = cj*cj*cmu
c1 = cj*cj*smu
c2 = cj*sj*cmu
c3 = cj*sj*smu
c4 = sj*cj*cmu
c5 = sj*cj*smu
c6 = sj*sj*cmu
c7 = sj*sj*smu

print(f"N: {N}, J = {J}, mu = {mu}")

a5 = -c2*(c0-c6)/(-2.*c2**2+c0*c6+c6**2)
b5 = -(c0**2-2.*c2**2+c0*c6)/(-2.*c2**2+c0*c6+c6**2)
d5 = -c2*(c0-c6)/(-2.*c2**2+c0*c6+c6**2)

a6 = (2.*c2**2-c0*c6-c6**2)/c2/(c0-c6)
b6 = 1.
d6 = (c0**2-2.*c2**2+c0*c6)/c2/(c0-c6)

a7 = (c0*c2-c2*c6)/(c0**2-2.*c2**2+c0*c6)
b7 = (2.*c2**2-c0*c6-c6**2)/(c0**2-2.*c2**2+c0*c6)
d7 = (c0*c2-c2*c6)/(c0**2-2.*c2**2+c0*c6)

a8 = (c0**2-2.*c2**2+c0*c6)/c2/(c0-c6)
b8 = 1.
d8 = (2.*c2**2-c0*c6-c6**2)/c2/(c0-c6)


# Prepare eigen vectors in N
ev = [[0 for _ in range(N)] for _ in range(N)]
for k in range(N):
    ev[k][k] = 1

def bintoint(i):
    n = len(i) 
    int = 0
    for k in range(n):
        int += i[n-k-1] * 2**k
    return int

def mod2(i):
    n = len(i)
    for k in range(n):
        i[k] = np.mod(i[k],2)
    return i


def normalize(v):
    norm = LA.norm(v)
    return v/norm

def sum(v):
    s = 0
    n = len(v)
    for k in range(n):
        s += v[k]
    return s 

#vector of expectation values of monoials
# V = np.zeros((1,2**N))
V = [0 for _ in range(2**N)]
V1 = [0 for _ in range(2**(N-1))]
for n in range(2**N):
    V[n] = bin(n)[2:].zfill(N)
 
count3 = 0   
for n1 in range(2**N):
    j = [int(d) for d in bin(n1)[2:].zfill(N)]
    s = sum(j)
    if s%2==0:
        V1[count3] = bin(n1)[2:].zfill(N)
        count3 += 1


# print(f"Notation of 2^N monomials:")
# print(V)
# print(V1)



#Let's compute W
W = np.zeros((N*2**N,2**N))

for l in range(2**N):        # i = (0,0,0,...,0,1) = "1", i = (1,1,...,1) = "2^N"
    i = [int(d) for d in bin(l)[2:].zfill(N)]
    for x in range(N):
        j = i[x]*2-1
        i0 = bintoint(i)
        W[l*N+x][i0] = (1. + j*c0)
        i1=bintoint(mod2(list(map(add, i, ev[np.mod(x,N)]))))
        W[l*N+x][i1] = j*c1
        i2=bintoint(mod2(list(map(add,list(map(add, i, ev[x])),ev[np.mod(x+1,N)]))))
        W[l*N+x][i2] = j*c2
        i3=bintoint(mod2(list(map(add, i, ev[np.mod(x+1,N)]))))
        W[l*N+x][i3] = j*c3
        i4=bintoint(mod2(list(map(add,list(map(add, i, ev[x])),ev[np.mod(x-1,N)]))))
        W[l*N+x][i4] = j*c4
        i5=bintoint(mod2(list(map(add, i, ev[np.mod(x-1,N)]))))
        W[l*N+x][i5] = j*c5
        i6=bintoint(mod2(list(map(add,list(map(add, i, ev[np.mod(x+1,N)])),ev[np.mod(x-1,N)]))))
        W[l*N+x][i6] = j*c6
        i7=bintoint(mod2(list(map(add,list(map(add,list(map(add, i, ev[np.mod(x+1,N)])),ev[np.mod(x-1,N)])),ev[x]))))
        W[l*N+x][i7] = j*c7

W0 = np.zeros((N*2**N,2**N))
for k in range(N*2**N):
    W0[k] = normalize(W[k])


W0s = np.transpose(W0)@W0

w, v = LA.eig(W0s)

# W matrix for only even monomials but with both even and odd basis
# We1 = np.zeros((N*2**(N-1),2**(N)))
# count1 = 0
# for l1 in range(2**N):        # i = (0,0,0,...,0,1) = "1", i = (1,1,...,1) = "2^N"
#     i = [int(d) for d in bin(l1)[2:].zfill(N)]
#     s = sum(i)
#     if s%2 ==0:
#         for x1 in range(N):
#             We1[count1] = W0[l1*N+x1]
#             count1 += 1

# W matrix for only even monomials with only even monomial basis
# We2 = np.zeros((N*2**(N-1),2**(N-1)))  
# count2 = 0
# for l2 in range(2**N):
#     i = [int(d) for d in bin(l2)[2:].zfill(N)]
#     s = sum(i)
#     if s%2 ==0:
#         We2[:,count2] = We1[:,l2]
#         count2 += 1

# Wes = np.transpose(We2)@We2
# we, ve = LA.eig(Wes)
     

w5 = sorted(abs(w))
print(f"Largest eigenvalue when v^2 = 1: {w5[2**(N)-1]}")
print(w5[0], w5[1], w5[2**N-1])
print(f"Condition number: {np.real(w5[1])/np.real(w5[2**N-1])}")

print("------------ Eigenvector of W^2 with small eigenvalues --------------")

for m in range(2**(N)-1):
    if np.real(w[m]) < 1e-3:
    # if np.real(w[m])/(np.real(v[0,m])**2) < 1e-3:
        print(f"{m}th eivenvalue:  {np.real(w[m]/w5[2**N-1])}")
        # print(np.real(v[:,m])/np.real(v[0,m]))


print("------------ Partial Orthonormalization --------------")

# The following is implemented only for N=3 case.
# G1 = np.zeros((2**N,2))
# G1[:,0] = np.transpose(W0[4])
# G1[:,1] = np.transpose(W0[7])
# q1, r1 = LA.qr(G1)


# W4 = np.zeros((N*2**N,2**N))
# for n3 in range(N*2**N):
#     W4[n3] = W0[n3]

# W4[4] = np.transpose(q1[:,0])
# W4[7] = np.transpose(q1[:,1])



# for n4 in range(N*2**N):
#     W4[n4] = normalize(W4[n4])

# W4s = np.transpose(W4)@W4

# w2, v2 = LA.eig(W4s)
# w6 = sorted(abs(w2))

# for m1 in range(2**N):
#     if w2[m1] < 1e-2:
#         print(f"{m1}th eivenvalue:  {w2[m1]/w6[2**N-1]}")

