#!/usr/bin/env python

import numpy as np

x = np.linspace(-4,4,100000)
part = np.trapz(np.exp(-x**2-x**4),x)
num2 = np.trapz(x**2*np.exp(-x**2-x**4),x)
num4 = np.trapz(x**4*np.exp(-x**2-x**4),x)
num6 = np.trapz(x**6*np.exp(-x**2-x**4),x)

part_free = np.trapz(np.exp(-x**2),x)
num2_free = np.trapz(x**2*np.exp(-x**2),x)
num4_free = np.trapz(x**4*np.exp(-x**2),x)
num6_free = np.trapz(x**6*np.exp(-x**2),x)

print(num2/part,num4/part,num6/part)
print(num2_free/part_free,num4_free/part_free,num6_free/part_free)
