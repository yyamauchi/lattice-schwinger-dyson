#Created on 4/22/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions with arbitrary # of sites
#, purely imaginary time
#aiming to make the code faster

# Files
import torch
from torch import nn
import numpy as np
import math
import random


# Physics setting
J = 0.4           #coupling
mu = 0.3            #magnetic
N = 8                #Number of temporal sites
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)
c0 = cj*cj*cmu
c1 = cj*cj*smu
c2 = cj*sj*cmu
c3 = cj*sj*smu
c4 = sj*cj*cmu
c5 = sj*cj*smu
c6 = sj*sj*cmu
c7 = sj*sj*smu

# Model, neural network
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(N,4*N),
            nn.ReLU(), 
            nn.Linear(4*N,4*N),
            nn.ReLU(),
            nn.Linear(4*N,1)
            )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations

triv = torch.zeros((1,N))

def loss_estimate(m, i, size, mode):

    x = np.random.randint(N)
    Pt = m(triv)
    Pt2 = m(triv)**2
    P0 = m(i)
    P = (c0 * P0 + c1 * m(np.mod(i+vec[x],2)) + c2 * m(np.mod(i+vec[x]+vec[np.mod(x+1,N)],2)) 
    + c3 * m(np.mod(i+vec[np.mod(x+1,N)],2)) + c4 * m(np.mod(i+vec[np.mod(x-1,N)]+vec[x],2))
    + c5 * m(np.mod(i+vec[np.mod(x-1,N)],2)) + c6 * m(np.mod(i+vec[np.mod(x-1,N)]+vec[np.mod(x+1,N)],2)) 
    + c7 * m(np.mod(i+vec[np.mod(x-1,N)]+vec[x]+vec[np.mod(x+1,N)],2)))

    tloss =0

    # takes care of the cases where i_x = 0/1 => -/+ without if statement
    j = (2. * i - 1.).float()

    for k in range(size):
        tloss += (P0[k] + j[k,x]*P[k])**2/Pt2


    if mode ==1:
        print(x)
        # print(loss)

    # When mu=0, impose <s1> + ... + <sn> = 0
    if mode ==2:
        lsa = m(vec[0]) + m(vec[1]) + m(vec[2])
        tloss = tloss + 10*N * c0 * abs(lsa)/float(size)


    return tloss


# functions for training and testing
vec = torch.zeros((N,1,N,))
for k in range(N):
    vec[k,0,k] = 1

trainsize = N * 25

def train_loop(m, optimizer):
    #Create random monomial
    mn = torch.zeros((trainsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(trainsize,N)))

    # Compute loss and optimize
    optimizer.zero_grad()
    Tloss = loss_estimate(m, mn, trainsize,0)
    Tloss.backward()
    optimizer.step()

testsize = N * 100

def test_loop(m):
	#Create random monomial
    mn = torch.zeros((testsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(testsize,N)))

    # Compute loss and optimize for N times (with different x) with 100 monomials
    Loss = 0
    for _ in range(N):
        Loss += loss_estimate(m, mn, testsize,0)/float(N)

    print(Loss)
    return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
goal = 100.0
trial = 0

input = torch.zeros((4,N,))
input[1,N-1] = 1.
input[2,N-1] = 1.
input[2,N-2] = 1.
for i in range(N):
    input[3,i] = 1.

print(input)

while goal > 1e-3:
	trial += 1
	train_loop(model, optimizer)
	if trial % 1000 == 0:
		print(f"{trial}th loop ------------------")
		goal = test_loop(model)
		pred = model(input)
		print(pred[0].item(),pred[1].item()/pred[0].item(),pred[2].item()/pred[0].item(),pred[3].item()/pred[0].item())







