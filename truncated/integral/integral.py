#!/usr/bin/env python

import torch

lamda = 1.

K = 12

expectations = torch.nn.Parameter(torch.zeros(K+4))

opt = torch.optim.Adam([expectations], lr=1e-3)

def cost_not_closed(exps):
    """
    The Schwinger-Dyson equations are not formally closed; thus they are
    underspecified, and many possible minima exist. Heuristically, the solver
    will favor solutions with small high-order expectation values. (I think.)
    """
    r = 0.
    e0 = - 2*expectations[1] - 4*lamda*expectations[3]**2
    e1 = 1 - 2*expectations[2] - 4*lamda*expectations[4]
    r += e0**2 + e1**2
    for m in range(2,K):
        e = m*expectations[m-1] - 2*expectations[m+1] - 4*lamda*expectations[m+3]
        r += e**2
    return r

cost = cost_not_closed

while True:
    cost_val = cost(expectations)
    opt.zero_grad()
    print(cost_val.item(), expectations[1].item(), expectations[2].item(), expectations[3].item(), expectations[4].item())
    cost_val.backward()
    opt.step()

