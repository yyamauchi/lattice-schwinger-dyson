#Created on 4/25/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions with arbitrary # of sites
#, purely imaginary time
#aiming to make the code faster by randomly combining Sd eqs and normalizing them

# Files
import torch
from torch import nn
import numpy as np
import math
import random
from random import gauss


# Physics setting
J = 0.6          #coupling
mu = 0.0            #magnetic
N = 3                #Number of temporal sites
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)
nr = 40       # number of random unit vectors  per sample
c0 = cj*cj*cmu
c1 = cj*cj*smu
c2 = cj*sj*cmu
c3 = cj*sj*smu
c4 = sj*cj*cmu
c5 = sj*cj*smu
c6 = sj*sj*cmu
c7 = sj*sj*smu


# Model, neural network
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(N,4*N),
            nn.ReLU(), 
            nn.Linear(4*N,4*N),
            nn.ReLU(),
            nn.Linear(4*N,1)
            )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations

triv = torch.zeros((1,N))
unitvec = [[0 for _ in range(N)] for _ in range(nr)]

def rand_unit_vector(n):
    vec = [gauss(0,1) for i in range(n)]
    return vec
    # mag = sum(x**2 for x in vec)**0.5
    # return [x/mag for x in vec]

def normalize(n):
    mag = sum(x**2 for x in n)**0.5
    return [x/mag for x in n]




def loss_estimate(m, i, size, mode):
    loss = torch.zeros((size,1))
    x = np.random.randint(N)

    #(x(delivative), trainsize index, spin index) => (x(delivative), trainsize index, 0)
    v = torch.zeros(16,size,N)

    v[0] = i
    v[1] = np.mod(i+vec[np.mod(x-2,N)],2)
    v[2] = np.mod(i+vec[np.mod(x-1,N)],2)
    v[3] = np.mod(i+vec[np.mod(x,N)],2)
    v[4] = np.mod(i+vec[np.mod(x+1,N)],2)
    v[5] = np.mod(i+vec[np.mod(x+2,N)],2)
    v[6] = np.mod(i+vec[np.mod(x-2,N)]+vec[np.mod(x-1,N)],2)
    v[7] = np.mod(i+vec[np.mod(x-2,N)]+vec[np.mod(x,N)],2)
    v[8] = np.mod(i+vec[np.mod(x-2,N)]+vec[np.mod(x-1,N)]+vec[np.mod(x,N)],2)
    v[9] = np.mod(i+vec[np.mod(x-1,N)]+vec[np.mod(x,N)],2)
    v[10] = np.mod(i+vec[np.mod(x-1,N)]+vec[np.mod(x+1,N)],2)
    v[11] = np.mod(i+vec[np.mod(x-1,N)]+vec[np.mod(x,N)]+vec[np.mod(x+1,N)],2)
    v[12] = np.mod(i+vec[np.mod(x,N)]+vec[np.mod(x+1,N)],2)
    v[13] = np.mod(i+vec[np.mod(x,N)]+vec[np.mod(x+2,N)],2)
    v[14] = np.mod(i+vec[np.mod(x,N)]+vec[np.mod(x+1,N)]+vec[np.mod(x+2,N)],2)
    v[15] = np.mod(i+vec[np.mod(x+1,N)]+vec[np.mod(x+2,N)],2)


    # takes care of the cases where i_x = 0/1 => -/+ without if statement
    j = (2. * i - 1).float()

    # The predicted 16 expectation values of basis <sigmas> when given the input i
    P = m(v)
    Pt = m(triv)
      
    # Create nr noumber of random vector in N - dimensional space
    for n in range(nr):
        unitvec[n] = rand_unit_vector(N)

    # For each input i, need to compute the sum of three vectors in W, compute SD and add it's abs
    tloss = 0

    for k in range(size):      # summing over randomly chosen monomials
        for l in range(nr):     # summing over randomly sampled unit vectors
            ls = 0.
            ja = j[k,np.mod(x-1,N)]*unitvec[l][0]
            jb = j[k,np.mod(x,N)]*unitvec[l][1]
            jc = j[k,np.mod(x+1,N)]*unitvec[l][2]
            w = [1. for _ in range(16)]
            w[0] = unitvec[l][0]+unitvec[l][1]+unitvec[l][2] + c0*(ja+jb+jc)
            w[1] = c5 * ja
            w[2] = ja*c1 + jb*c5
            w[3] = ja*c3 + jb* c1 + jc*c5
            w[4] = jb*c3 + jc*c1
            w[5] = jc*c3
            w[6] = ja*c4
            w[7] = ja*c6
            w[8] = ja*c7
            w[9] = ja*c2 + jb*c4
            w[10] = jb*c6
            w[11] = jb*c7
            w[12] = jb*c2 + jc*c4
            w[13] = jc*c6
            w[14] = jc*c7
            w[15] = jc*c2

            wn = normalize(w)
            for m in range(16):
                ls += wn[m] * P[m,k]/Pt
            tloss += abs(ls)/float(nr*size)




    if mode ==1:
        # print(x)
        print(tloss)


    return tloss


# functions for training and testing
vec = torch.zeros((N,N,))
for k in range(N):
    vec[k,k] = 1

trainsize = 4

def train_loop(m, optimizer):
    #Create random monomial
    mn = torch.zeros((trainsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(trainsize,N)))

    # Compute loss and optimize
    optimizer.zero_grad()
    Tloss = loss_estimate(m, mn, trainsize,0)
    Tloss.backward()
    optimizer.step()

testsize = 10

def test_loop(m):
	#Create random monomial
    mn = torch.zeros((testsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(testsize,N)))

    # Compute loss and optimize for N times (with different x) with 100 monomials
    Loss = 0
    for _ in range(N):
        Loss += loss_estimate(m, mn, testsize,0)/float(N)

    print(Loss)
    return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
goal = 100.0
trial = 0

input = torch.zeros((4,N,))
input[1,0] = 1.
input[2,0] = 1.
input[2,1] = 1.
for i in range(N):
    input[3,i] = 1.

print(input)

while goal > 1e-6:
	trial += 1
	train_loop(model, optimizer)
	if trial % 50 == 0:
		print(f"{trial}th loop ------------------")
		goal = test_loop(model)
		pred = model(input)
		print(pred[0].item(),pred[1].item()/pred[0].item(),pred[2].item()/pred[0].item(),pred[3].item()/pred[0].item())







