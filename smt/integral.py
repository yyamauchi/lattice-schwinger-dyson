#!/usr/bin/env python

import sys

# MODEL
# The action is x**2 / 2 + Lambda * x**4 / 4
Lambda = 1.

############
# NUMERICAL

import numpy as np

x = np.linspace(-6,6,1000000)
S = x**2 / 2. + Lambda * x**4/4.
part = np.trapz(np.exp(-S),x)
num2 = np.trapz(x**2*np.exp(-S),x)
num4 = np.trapz(x**4*np.exp(-S),x)
num6 = np.trapz(x**6*np.exp(-S),x)

print(num2/part,num4/part,num6/part)


#####
# Z3

import z3
z3.set_option(rational_to_decimal=True)
z3.set_option(precision=6)

# TRUNCATION
K = 13
Truncation = 'Experimental'
expect = [z3.Real(f'x^{i}') for i in range(K)]

if K % 2 == 0:
    print('K should be odd.')
    sys.exit(1)

# CONSTRAINTS
solver = z3.Solver()
solver.add(expect[0] == 1.)
solver.add(0 == -expect[1] - Lambda*expect[3])
for k in range(1,K-3):
    solver.add(0 == k*expect[k-1] - expect[k+1] - Lambda*expect[k+3])
solver.add(0 == expect[K-2])
if Truncation == 'Experimental':
    solver.add(0 == expect[K-1] - ((K-1)/2)**2 *expect[K-3])
elif Truncation == 'Naive':
    solver.add(0 == expect[K-1])
elif Truncation == 'Two-point':
    from scipy.special import factorial2
    solver.add(0 == expect[K-1] - factorial2(K-2)*expect[2])
elif Truncation == 'Max-point':
    solver.add(0 == expect[K-1] - ((K-1)/2)**2 *expect[K-3])
else:
    print('Unknown truncation requested')
    sys.exit(1)

#print(solver)
print(f'Check: {solver.check()}')

model = solver.model()
for term in expect[:9]:
    print(f'{term} = {model[term]}')

print()
print('STATISTICS')
for (k,v) in solver.statistics():
    print(f'{k} {v}')
