#Created on 5/3/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions with arbitrary # of sites
#, purely imaginary time, mu=0
#This code works only on even monomials

# Files
import torch
from torch import nn
import numpy as np
import math
import random

# Physics setting
J = 1.0          #coupling
N = 8                #Number of temporal sites
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
c0 = cj*cj
c2 = cj*sj
c6 = sj*sj

# Model, neural network
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten = nn.Flatten()
        self.layers = nn.Sequential(
            nn.Linear(N,4*N),
            nn.ReLU(), 
            nn.Linear(4*N,4*N),
            nn.ReLU(),
            nn.Linear(4*N,1)
            )

    def forward(self, x):
        # x = self.flatten(x)
        print(sum(x,N))        
        logits = self.layers(x)
        return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations

triv = torch.zeros((1,N))

def sum(v,leng):
    s = 0
    for k in range(leng):
        s += v[0,k]
    return s

def loss_estimate(m, i, size, mode):
    loss = torch.zeros((size,1))

    #(x(delivative), trainsize index, spin index) => (x(delivative), trainsize index, 0)
    v0 = torch.zeros(N,size,N)
    v2 = torch.zeros(N,size,N)
    v4 = torch.zeros(N,size,N)
    v6 = torch.zeros(N,size,N)
 
    for k1 in range(N):
        v0[k1] = i
        v2[k1] = np.mod(i+vec[k1]+vec[np.mod(k1+1,N)],2)
        v4[k1] = np.mod(i+vec[np.mod(k1-1,N)]+vec[k1],2)
        v6[k1] = np.mod(i+vec[np.mod(k1-1,N)]+vec[np.mod(k1+1,N)],2)

    Pt = m(triv)
    P0 = m(v0)
    P = c0 * P0 + c2 * m(v2) + c2 * m(v4) +  c6 * m(v6)

    j = (2. * i - 1).float()

    tloss =0

    for k2 in range(size):      # summing over randomly chosen monomials
        for n in range(N):      # summing over the site to take derivative at
            ls = (P0[n,k2,0] + j[k2,n]*P[n,k2,0])/Pt
            tloss += ls**2/float(N*size)

    # Mode 1 for printing 
    if mode ==1:
        print(x)
        # print(loss)

    return tloss


# functions for training and testing
vec = torch.zeros((N,N,))
for k in range(N):
    vec[k,k] = 1

trainsize = 5

def train_loop(m, optimizer):
    #Create random monomial
    mn = torch.zeros((trainsize,N,))
    count = 0
    while count < trainsize:
        mnt = torch.tensor(np.random.randint(2,size=(1,N)))
        s = sum(mnt,N)
        if s%2 ==0:
            mn[count] = mnt
            count += 1
 
    # Compute loss and optimize
    optimizer.zero_grad()
    Tloss = loss_estimate(m, mn, trainsize, 0)
    Tloss.backward()
    optimizer.step()

testsize = 100

def test_loop(m):
    #Create random monomial
    mn = torch.zeros((testsize,N,))
    count = 0
    while count < testsize:
        mnt = torch.tensor(np.random.randint(2,size=(1,N)))
        s = sum(mnt,N)
        if s%2 ==0:
            mn[count] = mnt
            count += 1

    # Compute loss and optimize for N times (with different x) with 100 monomials
    Loss = 0
    for _ in range(N):
        Loss += loss_estimate(m, mn, testsize,0)/float(N)

    return(Loss)



# Let's run the training
learning_rate1 = 1e-3
optimizer1 = torch.optim.Adam(model.parameters(), lr = learning_rate1)
goal = 100.0
trial = 0

input = torch.zeros((4,N,))
input[1,N-1] = 1.
input[1,N-2] = 1.
input[2,N-1] = 1.
input[2,N-3] = 1.
for i in range(N):
    input[3,i] = 1.

print(input)

# model(input)

# while goal > 1e-5:
#     trial += 1
#     train_loop(model, optimizer1)
#     if trial % 200 == 0:
#         goal = test_loop(model)
#         print(f"---------- {trial}th loop, loss = {goal} ----------")
#         pred = model(input)
#         print(pred[0].item(),pred[1].item()/pred[0].item(),pred[2].item()/pred[0].item(),pred[3].item()/pred[0].item())



