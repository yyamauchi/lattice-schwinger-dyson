#!/usr/bin/env python

"""
TODO

get terms
evaluate <1> for normalization

save model
complex actions
command line arguments
more dimensions

get things to run on GPU
fix how random numbers are done
"""

import torch

# PHYSICS
V = 4
dim = 1  # Not actually a variable
m2 = 0.3
lamda = 0.3

# The interaction is lamda/4 * phi**4.

def terms(mon, x):
    """
    Takes a monomial and a site identifying a Schwinger-Dyson equation. (The
    site is the variable with respect to which differentiation is performed.)

    Returns two arguments. The first is a list of the monomials appearing in
    the corresponding S-D equation. The second is a list of their coefficients.
    """
    # The terms are: from original monomial, from kinetic (coupling two sites),
    # from mass and kinetic, and from coupling.

    return mon, [1., 1., m2+2*dim, lamda]

# MODEL
W = 20

model = torch.nn.Sequential(
    torch.nn.Linear(V,W),
    torch.nn.Sigmoid(),
    torch.nn.Linear(W,1),
)

opt = torch.optim.Adam(model.parameters(), lr=1e-3)

def estimate_loss(m, K=100):
    # Construct the generating monomials
    gen = torch.poisson(torch.ones((K,V))*(3/V))
    # TODO
    loss = sum(abs(m(gen)))
    return loss

for _ in range(1000):
    loss = estimate_loss(model)
    opt.zero_grad()
    print(loss.item())
    loss.backward()
    opt.step()

