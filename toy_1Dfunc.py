# Solving 1D function via pytorch

# Files
import os
import torch
from torch import nn
import numpy as np
import math
import random

device = 'cuda' if torch.cuda.is_available() else 'cpu'
# print('Using {} device'.format(device))

# Constraints on the model
def f(x):
	return 1.2 + 2.5*x + x*x

rng = 8

# Model, neurral network
class Model(nn.Module):
	def __init__(self):
		super(Model, self).__init__()
		self.flatten = nn.Flatten()
		self.linear_relu_stack = nn.Sequential(
            nn.Linear(1,10),
            nn.ReLU(),
            nn.Linear(10,1)
		)

	def forward(self, x):
		x = self.flatten(x)
		logits = self.linear_relu_stack(x)
		return logits


# Create instance
model = Model()


# functions for training and testing
x = torch.zeros((rng,1,))
for n in range(rng):
	x[n,0] = float(n)

fx = torch.zeros((rng,1,))
for n in range(rng):
	fx[n,0] = f(n)

# def loss_fn(rng, dif):   # backword() needs to be defined
# 	loss = 0
# 	for range in (rng):
# 		loss += math.sqrt(dif[0,n]**2)

# 	return(loss)


def train_loop(size, model, loss_fn, optimizer):
	optimizer.zero_grad()
	pred = model(x)
	loss = loss_fn(pred, fx)
	loss.backward()
	optimizer.step()

def test_loop(model):
	dif = model(x) - fx
	test_loss = 0
	for n in range(rng):
		test_loss += math.sqrt(dif[n,0]**2)
	print("test_loop", test_loss/float(rng))
	return test_loss/float(rng)


# Let's run the training
learning_rate = 3e-4
loss_fn = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate)

goal = 100.0
trial = 0
while goal > 1e-0:
	trial += 1
	train_loop(rng,model,loss_fn,optimizer)
	if trial % 1000== 0:
		print(f"Trial {trial} ------------------")
		goal = test_loop(model)
		print(fx)
		print(model(x))

