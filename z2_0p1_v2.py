#Created on 4/24/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions with arbitrary # of sites
#, purely imaginary time
#aiming to make the code faster, especially in large J regime by introducing random unit vectors

# Files
import torch
from torch import nn
import numpy as np
import math
import random
from random import gauss


# Physics setting
J = 0.8             #coupling
mu = 0.0            #magnetic
N = 3               #Number of temporal sites
nr = 10             # number of random unit vectors  per sample

cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)
c0 = cj*cj*cmu
c1 = cj*cj*smu
c2 = cj*sj*cmu
c3 = cj*sj*smu
c4 = sj*cj*cmu
c5 = sj*cj*smu
c6 = sj*sj*cmu
c7 = sj*sj*smu

# Model, neural network
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(N,4*N),
            nn.ReLU(), 
            nn.Linear(4*N,4*N),
            nn.ReLU(),
            nn.Linear(4*N,1)
            )

    def forward(self, x):
        logits = self.linear_relu_stack(x)
        return logits

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations

triv = torch.zeros((1,N))
def rand_unit_vector(n):
    vec = [gauss(0,1) for i in range(n)]
    mag = sum(x**2 for x in vec)**0.5
    return [x/mag for x in vec]

unitvec = [[0 for _ in range(N)] for _ in range(nr)]



def loss_estimate(m, i, size, mode):
    loss = torch.zeros((size,1))

    #(x(delivative), trainsize index, spin index) => (x(delivative), trainsize index, 0)
    v0 = torch.zeros(N,size,N)
    v1 = torch.zeros(N,size,N)
    v2 = torch.zeros(N,size,N)
    v3 = torch.zeros(N,size,N)
    v4 = torch.zeros(N,size,N)
    v5 = torch.zeros(N,size,N)
    v6 = torch.zeros(N,size,N)
    v7 = torch.zeros(N,size,N)
 
    for j in range(N):
        v0[j] = i
        v1[j] = np.mod(i+vec[j],2)
        v2[j] = np.mod(i+vec[j]+vec[np.mod(j+1,N)],2)
        v3[j] = np.mod(i+vec[np.mod(j+1,N)],2)
        v4[j] = np.mod(i+vec[np.mod(j-1,N)]+vec[j],2)
        v5[j] = np.mod(i+vec[np.mod(j-1,N)],2)
        v6[j] = np.mod(i+vec[np.mod(j-1,N)]+vec[np.mod(j+1,N)],2)
        v7[j] = np.mod(i+vec[np.mod(j-1,N)]+vec[j]+vec[np.mod(j+1,N)],2)

    Pt = m(triv)
    P0 = m(v0)
    P = c0 * P0 + c1 * m(v1) + c2 * m(v2) + c3 * m(v3) + c4 * m(v4) + c5 * m(v5) + c6 * m(v6) + c7 * m(v7)


    tloss =0

    for j in range(nr):
        unitvec[j] = rand_unit_vector(N)

    for k in range(size):      # summing over randomly chosen monomials
        for l in range(nr):     # summing over randomly sampled unit vectors
            for n in range(N): # summing over x in unit vector
                ls = 0
                if i[k,n]==0:
                    ls += unitvec[l][n]*(P0[n,k,0]-P[n,k,0])/Pt
                else:
                    ls += unitvec[l][n]*(P0[n,k,0]+P[n,k,0])/Pt
                tloss += abs(ls)/float(5*size)




    if mode ==1:
        print(x)
        # print(loss)

    # When mu=0, impose <s1> + ... + <sn> = 0
    if mode ==2:
        lsa = m(vec[0]) + m(vec[1]) + m(vec[2])
        tloss = tloss + 10*N * c0 * abs(lsa)/float(size)


    return tloss


# functions for training and testing
vec = torch.zeros((N,N,))
for k in range(N):
    vec[k,k] = 1

trainsize = 40

def train_loop(m, optimizer):
    #Create random monomial
    mn = torch.zeros((trainsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(trainsize,N)))
 
    # Compute loss and optimize
    optimizer.zero_grad()
    Tloss = loss_estimate(m, mn, trainsize,0)
    Tloss.backward()
    optimizer.step()

testsize = 40

def test_loop(m):
	#Create random monomial
    mn = torch.zeros((testsize,N,))
    mn[:,:] = torch.tensor(np.random.randint(2,size=(testsize,N)))

    # Compute loss and optimize for N times (with different x) with 100 monomials
    Loss = 0
    for _ in range(N):
        Loss += loss_estimate(m, mn, testsize,0)/float(N)

    print(Loss)
    return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)
goal = 100.0
trial = 0

input = torch.zeros((4,N,))
input[1,0] = 1.
input[2,0] = 1.
input[2,1] = 1.
for i in range(N):
    input[3,i] = 1.

print(input)

while goal > 1e-3:
	trial += 1
	train_loop(model, optimizer)
	if trial % 200 == 0:
		print(f"{trial}th loop ------------------")
		goal = test_loop(model)
		pred = model(input)
		print(pred[0].item(),pred[1].item()/pred[0].item(),pred[2].item()/pred[0].item(),pred[3].item()/pred[0].item())







