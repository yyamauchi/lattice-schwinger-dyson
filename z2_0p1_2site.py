#Created on 4/29/2021
#ML code for solving Z2 lattice Schwinger-Dyson equations in 0+1 dimensions, purely imaginary time


# Files
import torch
from torch import nn
import numpy as np
import math
import random


# Physics setting
J = 1.0             #coupling
mu = 0.2             #magnetic
N = 2                #Number of temporal sites
cj = math.cosh(-2.*J)
sj = math.sinh(-2.*J)
cmu = math.cosh(-2.*mu)
smu = math.sinh(-2.*mu)

# Model, neural network
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(N,20),
            nn.ReLU(),
            nn.Linear(20,1)
        )

    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits
        # if x[0,0] == 0. and x[0,1] == 0.:
        #     return logits
        # return logits*1e-2

# Create instance
model = Model()

# Constraints on the model - Schwinger-Dyson equations
def loss_estimate(m, i, size, mode):

    loss = torch.zeros((size,1))

    for k in range(size):
        x = np.random.randint(N)
        iv = i[k]
        v1 = np.mod(iv+vec[x],2)
        v2 = np.mod(iv+vec[x]+vec[np.mod(x+1,N)],2)
        v3 = np.mod(iv+vec[np.mod(x+1,N)],2)
        if i[k,0,x]==0:
            loss[k,0] = ((1.-cj*cmu)*m(iv) - cj*smu*m(v1) - sj*cmu*m(v2) - sj*smu*m(v3))/m(torch.tensor([[0.,0.]]))
        else:
            loss[k,0] = ((1.+cj*cmu)*m(iv) + cj*smu*m(v1) + sj*cmu*m(v2) + sj*smu*m(v3))/m(torch.tensor([[0.,0.]]))

    if mode == 1:
        print(loss)
    tloss =0
    for j in range(size):
        tloss += abs(loss[j,0])/float(size)

    return tloss


# functions for training and testing
vec = torch.zeros((N,1,N,))
for k in range(N):
    vec[k,0,k] = 1

trainsize = 50

def train_loop(m, optimizer):
    #Create random monomial
    mn = torch.zeros((trainsize,1,N,))
    mn[:,0,:] = torch.tensor(np.random.randint(2,size=(trainsize,N)))
    #for i in range(trainsize):
    #    for j in range(N):
    #        mn[i,0,j] = np.random.randint(2)


    # Compute loss and optimize
    optimizer.zero_grad()
    Tloss = loss_estimate(m, mn, trainsize,0)
    Tloss.backward()
    optimizer.step()

testsize = 500

def test_loop(m):
    #Create random monomial
    mn = torch.zeros((testsize,1,N,))
    mn[:,0,:] = torch.tensor(np.random.randint(2,size=(testsize,N)))
    #for i in range(testsize):
    #    for j in range(N):
    #        mn[i,0,j] = np.random.randint(2)

    # Compute loss and optimize
    Loss = loss_estimate(m, mn, testsize,0)

    print(Loss)
    return(Loss)



# Let's run the training
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr = learning_rate)

goal = 100.0
trial = 0
input = torch.tensor([[[0.,0.]],[[0.,1.]],[[1.,0.]],[[1.,1.]]])

while goal > 1e-4:
    trial += 1
    train_loop(model, optimizer)
    if trial % 10== 0:
        print(f"{trial}th loop ------------------")
        goal = test_loop(model)
        pred = model(input)
        #print(pred[0].item(),pred[1].item(),pred[2].item(),pred[3].item())
        print(pred[0].item(), pred[1].item()/pred[0].item(), pred[2].item()/pred[0].item(), pred[3].item()/pred[0].item())







